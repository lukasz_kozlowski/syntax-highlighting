" Vim syntax file
" Language:     Trurl
" Filenames:    *.trurl
" Maintainers:  Łukasz Kozłowski
" URL:          https://bitbucket.org/lukasz_kozlowski/syntax-highlighting

if exists("b:current_syntax")
    finish
endif

syn region trurlComment start="//" end="$"
syn keyword trurlDefinition fun macro type val
syn keyword trurlControlFlowKeywords if then else
syn keyword trurlSelfSpecialIdentifier self Self
syn keyword trurlStandardMacros import quote
syn region trurlTypeParameters start=/</ end=/>/ contains=CONTAINED
syn keyword trurlBuiltinMetatypes struct class enum variant external
syn keyword trurlBuiltinTypes Unit Bool Double
syn keyword trurlBuiltinTypes Int Int8 Int16 Int32 Int64 UInt
syn keyword trurlBuiltinTypes Byte ByteArray String StringBuilder
syn keyword trurlBuiltinTypes Option Array List Result Nothing
syn keyword trurlBuiltinTypes Ast
syn region trurlStringLiteral start=/"/ skip=/\\./ end=/"/ contains=CONTAINED
syn match trurlIntegerLiteral /\<\d\(_\|\d\)*\>/
syn keyword trurlBoolLiteral true false

hi link trurlComment Comment
hi link trurlDefinition Operator
hi link trurlControlFlowKeywords Keyword
hi link trurlSelfSpecialIdentifier Keyword
hi link trurlStandardMacros PreProc
hi link trurlBuiltinMetatypes Special
hi link trurlBuiltinTypes Type
hi link trurlStringLiteral String
hi link trurlIntegerLiteral Number
hi link trurlBoolLiteral Boolean

let b:current_syntax = "Trurl"

