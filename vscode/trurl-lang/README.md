# trurl-lang README

## Features

Syntax highlighting for Trul language.

## Release Notes

### 0.0.1

Initial support for syntax highlighting.
